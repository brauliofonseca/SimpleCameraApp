package com.cs185.brauliofonseca.braulio00cameraroll1;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;

/**
 * Created by pop on 5/11/16.
 */
public class DummyDialog extends DialogFragment {

    @Override
    public Dialog onCreateDialog (Bundle savedInstanceState) {
        //Create builder
        AlertDialog.Builder popupWindow = new AlertDialog.Builder(getActivity());

        // Inflate the dummy window
        View contentView = getActivity().getLayoutInflater().inflate(R.layout.dummy_dialog_main, null);

        popupWindow.setView(contentView)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Does not do anything
                    }
                });

        return popupWindow.create();
    }
}
