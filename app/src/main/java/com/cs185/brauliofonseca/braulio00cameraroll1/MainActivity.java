package com.cs185.brauliofonseca.braulio00cameraroll1;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
    private static final String VERIFY_URI_TAG = "VerifyURI";
    private Uri fileUri;
    private static int photo_count = 0;
    private ArrayList<Uri> uriList;
    private ImageAdapter myAdapter;
    private GridView myGridView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        int writePermission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (writePermission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        }

        int readPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        if (readPermission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
        }

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                executeCameraIntent();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        uriList = new ArrayList<Uri>();
        myAdapter = new ImageAdapter(this, uriList);


        myGridView = (GridView) findViewById(R.id.gridView);
        myGridView.setAdapter(myAdapter);


        myGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ImageFull full_screen_image = new ImageFull();
                Intent intent = new Intent(MainActivity.this, ImageFull.class);
                intent.putExtra("im_uri", myAdapter.getItem(position));
                startActivity(intent);
            }

        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Confirm Deletion");
            builder.setMessage("Are you sure?");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    deleteImages();
                    dialog.dismiss();
                }
            });
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.folder_view) {
            // Handle the camera action
            DummyDialog new_window = new DummyDialog();
            new_window.show(getFragmentManager(), "dummy_window");
        } else if (id == R.id.list_view) {
            DummyDialog new_window = new DummyDialog();
            new_window.show(getFragmentManager(), "dummy_window");

        } else if (id == R.id.settings_sel) {
            DummyDialog new_window = new DummyDialog();
            new_window.show(getFragmentManager(), "dummy_window");

        } else if (id == R.id.help_sel) {
            DummyDialog new_window = new DummyDialog();
            new_window.show(getFragmentManager(), "dummy_window");

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Log.d(VERIFY_URI_TAG, fileUri.toString());
                if(Uri.EMPTY.equals(fileUri)) {
                    Log.d(VERIFY_URI_TAG, "It is empty");
                }
                myAdapter.addUri(fileUri);
                myAdapter.notifyDataSetChanged();

            } else if (resultCode == RESULT_CANCELED) {
                // User cancelled the capture
                System.out.print(fileUri);

            } else {
                // Image fail captured
                System.out.print(fileUri);

            }
        }
    }

    private void executeCameraIntent() {

        // Create the intent to take a picture
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        // Create a file to save the image
        fileUri = getOutputCameraFileUri();
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // Start the intent
        startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);

    }


    private Uri getOutputCameraFileUri() {
        return Uri.fromFile(getOutputCameraFile());
    }

    private File getOutputCameraFile() {
        File image = null;
        String filename = "photo-" + Integer.toString(photo_count) + ".jpg";
        photo_count += 1;

        File imageStorageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);


        if (! imageStorageDir.exists()) {
            if (! imageStorageDir.mkdirs()) {
                Log.d("MyApp", "failed to create directory");
                return null;
            }
        }

        try {
            image = new File(imageStorageDir,  filename);
            image.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return image;
    }

    public void deleteImages() {
        photo_count = 0;
        myAdapter.resetList();
        myAdapter.notifyDataSetChanged();
        myGridView.invalidateViews();
        myGridView.setAdapter(myAdapter);

    }
}
