package com.cs185.brauliofonseca.braulio00cameraroll1;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import java.util.ArrayList;

/**
 * Created by pop on 5/11/16.
 */
public class ImageAdapter extends BaseAdapter{

    private Context mContext;
    private ArrayList<Uri> imageList;
    private static String ADAPTER_TEST_TAG = "TestAdapter";

    // Constructor method
    public ImageAdapter(Context c, ArrayList<Uri> uriList) {
        mContext = c;
        imageList = uriList;
    }

    public void addUri(Uri newImage) {
        Log.d(ADAPTER_TEST_TAG, "In the add function");
        imageList.add(0, newImage);
        Log.d(ADAPTER_TEST_TAG, "Add successful");
    }

    public void resetList() {
        imageList.clear();
    }

    @Override
    public int getCount() {
        return imageList.size();
    }

    @Override
    public Uri getItem(int position) {
        return (imageList.get(position));
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;

        if (convertView == null) {
            imageView = new ImageView(mContext);
            imageView.setLayoutParams(new GridView.LayoutParams(200,200));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(10, 10, 10, 10);
        } else {
            imageView = (ImageView) convertView;
        }


        Log.d("TestAdapter", imageList.get(position).toString());


//        Bitmap bm = bitMapFromUri(imageList.get(position), 220, 220);
//        imageView.setImageBitmap(bm);
        imageView.setImageURI(imageList.get(position));

        return imageView;
    }


    public Bitmap bitMapFromUri(Uri imageUri, int reqWidth, int reqHeight) {
        Bitmap bm = null;
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(imageUri.getPath(), options);

        options.inSampleSize = calculateSampleSize(options, reqWidth, reqHeight);

        options.inJustDecodeBounds = false;
        bm = BitmapFactory.decodeFile(imageUri.toString(), options);

        return bm;
    }

    public int calculateSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            if (width > height) {
                inSampleSize = Math.round((float)height / (float)reqHeight);
            } else {
                inSampleSize = Math.round((float) width / (float) reqWidth);
            }
        }

        return inSampleSize;
    }
}
